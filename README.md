# ssm-eclipse-chapter13

#### 介绍
第14章 JSON数据交互和RESTful支持

#### 推荐在线工具资料

天气API

https://v0.yiketianqi.com/api?version=v61&appid=39974654&appsecret=JCy5INEE

JSON 相关资料推荐

https://www.bejson.com/

接口测试工具 RunAPI在线版

http://runapi.showdoc.cc/#/

注意：

为了方便本地测试，springmvc-config.xml 中添加信息注意删除

#### 说明

项目运行 Run on Server

- P218

![输入图片说明](https://images.gitee.com/uploads/images/2021/0426/201144_20e74cdd_382074.png "屏幕截图.png")

- 参考实例: P223 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0426/201401_36c95965_382074.png "屏幕截图.png")
 
- 在线测试：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0426/215617_a1a04e08_382074.png "屏幕截图.png")

